class Course < ActiveRecord::Base
  belongs_to :category
  has_many :articles

  scope :category_with, lambda { |category| where(:category_id => category) }

  attr_accessible :description, :title, :category_id, :tag_list
  acts_as_taggable
end
