class Category < ActiveRecord::Base
  has_many :articles
  has_many :courses

  attr_accessible :name, :type_name

  default_scope order(:id)
  scope :for_article, where(:type_name => "Article")
  scope :for_course, where(:type_name => "Course")

  TYPES = ["Article", "Course"]
end
