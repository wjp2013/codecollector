class Article < ActiveRecord::Base
  belongs_to :category
  belongs_to :course

  # default_scope order("updated_at DESC")
  scope :category_with, lambda { |category| where(:category_id => category) }

  attr_accessible(
    :content,
    :summary,
    :title,
    :category_id,
    :course_id,
    :screenshot,
    :video,
    :tag_list,
    :privated
  )
  mount_uploader :screenshot, ImageUploader
  acts_as_taggable
end
