# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
jQuery ->
  tag_it = (tags, element) ->
    $this = tags
    $("span", $this).each ->
      $tag = $(this)
      $tag.click ->
        $tag.toggleClass "label-success"
        tag_list = ""
        $("span.label-success", $this).each ->
          tag_list += $(this).html() + " "

        element.val tag_list

  tag_it $("#all_tags_for_course"), $("#course_tag_list")
