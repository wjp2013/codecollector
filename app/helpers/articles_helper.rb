module ArticlesHelper
  class HTMLwithPygments < Redcarpet::Render::HTML
    def block_code(code, language)
      sha = Digest::SHA1.hexdigest(code)
      Rails.cache.fetch ["code", language, sha].join('-') do
        Pygments.highlight(code, lexer: language, options: { linespans: 'line' })
      end
    end

    # Extend the Redcarpet 2 Markdown library.
    # http://dev.af83.com/2012/02/27/howto-extend-the-redcarpet2-markdown-lib.html
    # Inlcude ActionView::Helpers::TagHelper so we can use content_tag method.
    include ActionView::Helpers::TagHelper
    def block_quote(quote)
      content_tag(:blockquote, content_tag(:div, quote.html_safe, class: 'quote'))
    end
  end

  def markdown(text)
    renderer = HTMLwithPygments.new(hard_wrap: true, filter_html: true)
    options = {
      autolink: true,
      no_intra_emphasis: true,
      fenced_code_blocks: true,
      lax_html_blocks: true,
      strikethrough: true,
      superscript: true
    }
    # If text(means article content) is nil, there will be wrong argument type nil (expected String).
    # so make text || ""
    Redcarpet::Markdown.new(renderer, options).render(text || "").html_safe
  end

  def layout_with_screenshot(article)
    "span10" if article.screenshot?
  end

  def show_tag_name_warp_by_span(tag_list, tag)
    class_name = tag_list.include?(tag.name) ? "label label-success" : "label"
    content_tag(:span, tag.name, :class => class_name)
  end

  def tag_list_with_link(article)
    text =  ""
    article.tags.each do |tag|
      text << "#{link_to tag.name, articles_path(tag: tag.name)}"
    end
    text.html_safe
  end
end
