module ApplicationHelper
  def show_syntax_cheatsheet?
    if controller.controller_name == "articles"
      if controller.action_name == "new" || controller.action_name == "edit"
        return true
      end
    end
    false
  end

  def show_categories?
    !show_syntax_cheatsheet?
  end

  def link_with_tag(articles, courses, tag)
    count = show_count(articles, courses, tag)
    unless count.zero?
      # NOTE:: we want to go to article list by tag any where.
      # if articles # if on the home page or article listing page.
      #   text = link_to tag, articles_path(category: params[:category], tag: tag)
      # else # if on the course listing page.
      #   text = link_to tag, courses_path(category: params[:category], tag: tag)
      # end
      text = link_to tag, articles_path(category: params[:category], tag: tag)
      content_tag(:li, text + content_tag(:span, count, class: 'badge'))
    end

  end

  def show_count(articles, courses, tag)
    if articles # if on the home page or article listing page.
      objs = articles ? articles : Article
    elsif courses # if on the course listing page.
      objs = courses ? courses : Course
    else # else maybe in article or course show page.
      objs = Article
    end
    objs.tagged_with(tag).count
  end

  def authorize?
    request.remote_ip == "127.0.0.1"
  end

end

