# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
courses = Course.create([{ title: 'jQuery UI 101: The Essentials', description: 'some text for course.' }, 
  { title: 'Create a Set of Coffee and Tea Icons in Adobe Illustrator', description: 'some text for course.' }])
categories = Category.create([{ name: 'Ruby on Rails', type_name: 'Course' }, { name: 'JavaScript', type_name: 'Article' }])
category = Category.create({ name: 'Test', type_name: 'Article' })
20.times.each do |i|
  Article.create({
  :title => "Test Article",
  :category_id => category.id,
  :summary => %Q(In this course, we take a look at every single widget in jQuery UI. We look through the APIs of each widget, learn how they function, review common implementations, and so much more.),
  :content => 
%Q(
*italic*   **bold**

An [example](http://url.com/ "Title")

## Header 2

WYSIWYG editing in the browser is still difficult to integrate or extend, and generated markup is often suspect. In the meantime a crop of text formatting languages such as Markdown, Textile, BBCode and wiki markup have emerged as ways for users to enter rich content in a controlled enviornment.
### Header 3
#### Header 4
##### Header 5
###### Header 6

1. Foo
2.  Bar

*   A list item.
    With multiple paragraphs.
*   Bar

*   Abacus
    * answer
*   Bubbles
    1.  bunk
    2.  bupkis
        * BELITTLER
    3. burper
*   Cunning

> Email-style angle brackets
> are used for blockquotes.
> #### Headers in blockquotes
> 
> * You can quote a list.
> * Etc.


* * *

Roses are red,   Violets are blue.

WYSIWYG editing in the browser is still difficult to integrate or extend, and generated markup is often suspect. In the meantime a crop of text formatting languages such as Markdown, Textile, BBCode and wiki markup have emerged as ways for users to enter rich content in a controlled enviornment.

```css
pre {
  white-space: pre;
  background-color: #F3F3F3;
  border: solid 1px #CCC;
  padding: 5px 10px;
  font-family: 'Menlo', 'Courier New', 'Terminal', monospace;
}
```
hah

```ruby
class HTMLwithPygments < Redcarpet::Render::HTML
  def block_code(code, language)
    Pygments.highlight(code, lexer: language, options: { linespans: 'line' })      
    # sha = Digest::SHA1.hexdigest(code)
    # Rails.cache.fetch ["code", language, sha].join('-') do
    #   Pygments.highlight(code, lexer: language)
    # end
  end
end
```
)
  })
end