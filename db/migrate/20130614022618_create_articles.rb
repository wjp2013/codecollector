class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.string :video
      t.string :screenshot      
      t.text :summary
      t.text :content
      t.references :category
      t.references :course

      t.timestamps
    end
    add_index :articles, :title
  end
end
