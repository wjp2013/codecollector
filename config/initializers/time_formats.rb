# Adding your own time formats to to_formatted_s
# More Detail:
#   http://api.rubyonrails.org/classes/Time.html#method-i-to_formatted_s
#   https://github.com/rails/rails/blob/2-3-stable/activesupport/lib/active_support/core_ext/time/conversions.rb
#   http://jasonseifer.com/2010/03/10/rails-date-formats
# Time::DATE_FORMATS[:month_and_year] = "%B %Y"
# Time::DATE_FORMATS[:short_ordinal] = lambda { |time| time.strftime("%B #{time.day.ordinalize}") }
Time::DATE_FORMATS[:nice_time] = "%Y-%m-%d %H:%M"